# Rating Class

## Flow

## Diagram

### User Table
| objectId "String" | firstName "String" | avgRating "Object"                   |
| ----------------- |:------------------:| ------------------------------------:|
| wcDVB2vL53        | João Gabriel       | {"data":[1,0,0,0,0], "average":1.0}  |
| JXAN1KbC1d        | Marcia Regina      | (undefined)                          |

### Slot Table
| objectId "String" | name "String"   | userBy "Pointer" <_User> | avgRating "Object"                   |
| ----------------- |:---------------:|:------------------------:| ------------------------------------:|
| V8OkAbmc3k        | Vaga B1         | JXAN1KbC1d               | {"data":[1,0,0,0,0], "average":1.0}  |
  
### Rating Table
| objectId "String" | userBy "Pointer" <_User> | userTo "Pointer" <_User> | review "Pointer" <Review>  |
| ----------------- |:------------------------:|:------------------------:| --------------------------:|
| 6cudhWmCob        | wcDVB2vL53               | JXAN1KbC1d               | df7SqWbUsO                 |
| D0CB33KGQM        | JXAN1KbC1d               | wcDVB2vL53               | rDgwGovTzr                 |

### Review Table
| objectId "String" | rating "Object"                  | comment "String"                           | aproved "Boolean"  |
| ----------------- |:--------------------------------:|:------------------------------------------:| ------------------:|
| df7SqWbUsO        | {"average":1,"data":[1,1,1,1,1]} | Cachorro vulgo "Capiroto" riscou o carro!  | true               |
| rDgwGovTzr        | {"average":1,"data":[1,1,1,1,1]} | Carro vazando oleo! #Pobre                 | true               |


## Functions

## Rating

## Rating (User)

## Rating (Slot)