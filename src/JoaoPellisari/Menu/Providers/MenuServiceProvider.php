<?php

namespace JoaoPellissari\Menu\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class MenuServiceProvider extends BaseServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		 $this->mergeConfigFrom(__DIR__ . '/../../../../config/settings.php', 'joaopellissari.laravel-menu.settings');
		 $this->mergeConfigFrom(__DIR__ . '/../../../../config/views.php', 'joaopellissari.laravel-menu.views');
		 
		 $this->app->singleton('menu', function($app) {
		 	return new Menu;
		 });            
	}

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Extending Blade engine
		require_once(__DIR__.'/../blade/lm-attrs.php');
		$this->loadViewsFrom(__DIR__.'/../../../../resources/views', 'joaopellissari/laravel-menu');
		$this->publishes([
        	__DIR__ . '/../../../../resources/views' => base_path('resources/views/vendor/joaopellissari/laravel-menu'),
        	__DIR__ . '/../../../../config/settings.php' => config_path('joaopellissari/laravel-menu/settings.php'),
        	__DIR__ . '/../../../../config/views.php' => config_path('joaopellissari/laravel-menu/views.php'),
		]);
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('menu');
	}
}